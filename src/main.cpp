#include <iostream>

#include <GL/glew.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GLFW/glfw3.h>

uint32_t const FILL_COLOR = 0x365b3c;

// center_pos_x  = (win_width / 2) - (image_width / 2)
// center_pos_y  = (win_height / 2) - (image_height/ 2)

static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
}

void error_callback(int error, const char *description)
{
    fprintf(stderr, "ERROR: %s\n", description);
}

void exit_error(std::string const &message, bool terminate_glfw=true)
{
    std::cerr << "ERROR: " << message << '\n';

    if (terminate_glfw) {
        glfwTerminate();
    }

    std::exit(1);
}

void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
    glViewport(0, 0, width, height);
}

// normalized triangle coords
float vertices[] = {
    -0.5f, -0.5f, 0.0f,
     0.5f, -0.5f, 0.0f,
     0.0f,  0.5f, 0.0f,
};

const char *vertex_shader_source = 
"#version 330 core\n"
"layout (location = 0) in vec3 aPos;\n"
"\n"
"void main()\n"
"{\n"
"    gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
"}";

const char *frag_shader_source = 
"#version 330 core\n"
"out vec4 FragColor;\n"
"\n"
"void main()\n"
"{\n"
"    FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
"}";

int main()
{
    if (!glfwInit()) {
        exit_error("Could not init glfw\n", false);
    }
    glfwSetErrorCallback(error_callback);


    // this chunk segfaults
    unsigned int vertex_buffer_object;
    glGenBuffers(1, &vertex_buffer_object);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    unsigned int vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_shader_source, NULL);
    glCompileShader(vertex_shader);
    int success;
    char info_log[512];
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertex_shader, 512, NULL, info_log);
        std::cerr << "ERROR: shader failed to compile.\n" << info_log << '\n';
    }

    unsigned int frag_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(frag_shader, 1, &frag_shader_source, NULL);
    glCompileShader(frag_shader);
    glGetShaderiv(frag_shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertex_shader, 512, NULL, info_log);
        std::cerr << "ERROR: shader failed to compile.\n" << info_log << '\n';
    }

    unsigned int shader_program = glCreateProgram();
    glAttachShader(shader_program, vertex_shader);
    glAttachShader(shader_program, frag_shader);
    glLinkProgram(shader_program);
    glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shader_program, 512, NULL, info_log);
        std::cerr << "ERROR: failed to link shader program.\n" << info_log << '\n';
    } else {
        glUseProgram(shader_program);
    }
    glDeleteShader(vertex_shader);
    glDeleteShader(frag_shader);


    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

    int window_width = 1920, window_height = 1080;
    GLFWwindow *window = glfwCreateWindow(window_width, window_height, "BPM Cursor", NULL, NULL);
    if (!window) {
        exit_error("Could not create glfw window\n");
    }


    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetKeyCallback(window, key_callback);
    glfwSwapInterval(1);


    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);


    while(!glfwWindowShouldClose(window)) {
        double delta = glfwGetTime();
        //int frame_width, frame_height;
        //glfwGetFramebufferSize(window, &frame_width, &frame_height);
        //glViewport(0, 0, frame_width, frame_height);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }


    glfwDestroyWindow(window); // might not be freed if exit_error before
    glfwTerminate();
}

